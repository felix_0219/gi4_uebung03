section .data
	num1 DD 1, 4
	num2 DD 2, 9
	res DD 0, 0

section .text

global _start

_start:
	;Realpart of the result
	mov eax, [num1]
	mov ebx, [num2]
	mul ebx
	mov ecx, eax

	mov eax, [num1 + 4]
	mov ebx, [num2 + 4]
	mul ebx

	sub ecx, eax
	mov [res], ecx

	;Imaginerypart of the result
	mov eax, [num1]
	mov ebx, [num2 + 4]
	mul ebx
	mov ecx, eax

	mov eax, [num1 + 4]
	mov ebx, [num2]
	mul ebx

	add eax, ecx
	mov [res + 4], eax

	mov ebx, 0
	mov eax, 1
	syscall

section .data
x DW 4
coeff DD 33, 2, 7
result DW 0

section .text

global _start

_start:	
	mov ebx, coeff

	;al for multiplication
	mov ax, [x]
	mul ax
	mov rdx, [ebx]
	mul dx

	;saving the result
	mov cx, ax
	
	;al for multiplikation
	mov rax, [ebx + 4]
	mov dx, [x]
	mul dx

	
	add ax, cx
	add ax, [ebx+8]

	add ax, 65

	mov [result], ax
	
	mov rax, 1
	mov rdi, 1
	mov rsi, result
	mov rdx, 2
	syscall

	mov rax, 60
	mov rbx, 0
	syscall

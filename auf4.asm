section .data
x DW 2
coeff DD 3, 4, -5
result DW 0

section .text

global _start

_start:	
	mov rbx, coeff

	mov ax, [x]
	mov dx, [coeff]
	mul dx

	add ax, [coeff + 4]
	mov dx, [x]
	mul dx

	add ax, [coeff + 8]

	add ax, 65
	mov [result], ax
	
	mov rax, 1
	mov rdi, 1
	mov rsi, result
	mov rdx, 2
	syscall

	mov rax, 60
	mov rbx, 0
	syscall
